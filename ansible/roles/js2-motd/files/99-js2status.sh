#!/bin/sh

python_output=$(timeout 5 python3 -W ignore 2> /dev/null << EOF

# Read status from API into json object
from urllib.request import urlopen
import json
response = urlopen("https://7995836472657980.hostedstatus.com/1.0/status/61dc808a7e9a82053ce739d2", timeout=3)
json_data = json.loads(response.read())

# Change color of overall status message depending on status code
class ColorCodes:
	CYAN = "\033[96m"
	GREEN = "\033[92m"
	YELLOW = "\033[93m"
	RED = "\033[91m"
	ENDC = "\033[0m"
	BOLD = "\033[1m"

def getColorForStatus(status_code):
	color = ColorCodes.BOLD

	# Operational
	if status_code == 100:
		color += ColorCodes.GREEN

	# Scheduled Maintenance
	elif status_code == 200:
		color += ColorCodes.CYAN

	# Degraded Performance or Partial Outage
	elif status_code == 300 or status_code == 400:
		color += ColorCodes.YELLOW

	# Service Disrtuption or Security Issue
	elif status_code == 500 or status_code == 600:
		color += ColorCodes.RED

	return color

print("\n══════════════════════════https://jetstream.status.io/══════════════════════════\n")
print(f"Overall Jetstream2 Status:  {getColorForStatus(json_data['result']['status_overall']['status_code'])} {json_data['result']['status_overall']['status']} {ColorCodes.ENDC}\n")

# List out active incident items
if "incidents" in json_data["result"].keys() and len(json_data["result"]["incidents"]) > 0:
	print("Active Status Items:")
	for incident in json_data["result"]["incidents"]:
		incident_color = ""
		if "messages" in incident.keys() and len(incident["messages"]) > 0:
			incident_color = getColorForStatus(incident["messages"][-1]["status"])
		print(" ◦ ", incident_color, incident["name"], ColorCodes.ENDC)

print()
# List out active and planned maintenance
if len(json_data["result"]["maintenance"]["active"]) > 0:
	print(ColorCodes.YELLOW + ColorCodes.BOLD, "Jetstream2 is currently under active maintenance! See https://jetstream.status.io/ for more information.", ColorCodes.ENDC, "\n")

if len(json_data["result"]["maintenance"]["upcoming"]) > 0:
	print("Scheduled Maintenance:")
	for item in json_data["result"]["maintenance"]["upcoming"]:
		item_color = ""
		if "messages" in item.keys() and len(item["messages"]) > 0:
			item_color = getColorForStatus(item["messages"][-1]["status"])
		name = item["name"]
		start_datetime = item["datetime_planned_start"]
		end_datetime = item["datetime_planned_end"]
		print(" ◦ ", item_color, f"({start_datetime} - {end_datetime}) {name}", ColorCodes.ENDC)

print(("\n════════════════════════════════════════════════════════════════════════════════"))

EOF
)

python_return_code=$?
if [ $python_return_code -ne 0 ]; then
	echo
	echo "Visit https://jetstream.status.io/ for status information!"
else
	echo "${python_output}"
fi
echo
