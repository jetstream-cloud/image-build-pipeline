# Per https://lmod.readthedocs.io/en/latest/070_standard_modules.html
if [ -z "$__Init_Default_Modules" ]; then
   export __Init_Default_Modules=1;

   export LMOD_SYSTEM_DEFAULT_MODULES="xalt"

      if /usr/bin/timeout --foreground 5 /usr/bin/stat -f -t /software/ > /dev/null; then
          module --initial_load --no_redirect restore
      else
  echo "Module Initial Load: /software is not mounted correctly"
      fi
else
      if /usr/bin/timeout --foreground 5 /usr/bin/stat -f -t /software/ > /dev/null; then
            module -q refresh && export > /dev/null
      else
  echo "Module Refresh: /software is not mounted correctly"
      fi
fi
